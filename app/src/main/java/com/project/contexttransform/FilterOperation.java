package com.project.contexttransform;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Klasa do przekształceń kontekstowych
 */

public class FilterOperation {
    MathsOperation mo;
    public FilterOperation() {
        mo=new MathsOperation();
    }

    /**
     *
     * @param bitmap obraz
     * @param i wiersz pkt obrazu
     * @param j kolumna pkt obrazu
     * @param elementsList lista elementow poprzedniego pktu
     * @return nowa lista punktów
     */
    public List<Integer> choose9ElementsMask(Bitmap bitmap, int i, int j, List<Integer> elementsList) {

        if (j == 0 || j+1 == bitmap.getHeight()) {
            elementsList = new ArrayList<>();
            if (j == 0) {
                //1

                elementsList.add(0);

                //2
                elementsList.add(0);

                //3
                elementsList.add(0);
            } else {
                //1
                if (i == 0) {
                    elementsList.add(0);
                } else {
                    elementsList.add(bitmap.getPixel(i - 1, j - 1));
                }
                //2

                elementsList.add(bitmap.getPixel(i, j - 1));

                //3
                if ( i+1==bitmap.getWidth()) {
                    elementsList.add(0);
                } else {
                    elementsList.add(bitmap.getPixel(i + 1, j - 1));
                }

            }

            //4
            if (i == 0) {
                elementsList.add(0);
            } else {
                elementsList.add(bitmap.getPixel(i - 1, j));
            }
            //5-------
            elementsList.add(bitmap.getPixel(i, j));
            //-------
            //6
            if (i+1 == bitmap.getWidth()) {
                elementsList.add(0);
            } else {
                elementsList.add(bitmap.getPixel(i + 1, j));
            }
            if (j == 0) {
                //7
                if (i == 0) {
                    elementsList.add(0);
                } else {
                    elementsList.add(bitmap.getPixel(i - 1, j + 1));
                }

//8
                elementsList.add(bitmap.getPixel(i, j + 1));

//9
                if (i +1== bitmap.getWidth()) {
                    elementsList.add(0);
                } else {
                    elementsList.add(bitmap.getPixel(i + 1, j + 1));

                }

            }
            else
            {
                //7

                elementsList.add(0);

                //8
                elementsList.add(0);

                //9
                elementsList.add(0);
            }
        } else {
            elementsList.remove(0);
            elementsList.remove(0);
            elementsList.remove(0);
            //7
            if(i==0)
            {
                elementsList.add(0);
            }
            else
            {

                elementsList.add(bitmap.getPixel(i - 1, j + 1));
            }
            //8

            elementsList.add(bitmap.getPixel(i, j + 1));
            //9
            if(i+1==bitmap.getWidth())
            {
                elementsList.add(0);
            }else{
                elementsList.add(bitmap.getPixel(i + 1, j + 1));
            }

        }
        return elementsList;
    }

    /**
     * wybiera najwieksze wartosci
     * @param elementsList lista punktów maski
     * @return przetworzony punkt
     */
    public int maxPoint(List<Integer> elementsList) {
        List<Integer> r = new ArrayList<>();
        List<Integer> g = new ArrayList<>();
        List<Integer> b = new ArrayList<>();


        for (int elem : elementsList) {
            if (elem == 0) {
                r.add(0);
                g.add(0);
                b.add(0);
            } else {
                r.add(Color.red(elem));
                g.add(Color.green(elem));
                b.add(Color.blue(elem));
            }
        }

        return Color.rgb(mo.maxElemOfList(r), mo.maxElemOfList(g),mo.maxElemOfList(b));
    }
    /**
     * wybiera najmniejsze wartosci
     * @param elementsList lista punktów maski
     * @return przetworzony punkt
     */
    public int minPoint(List<Integer> elementsList) {
        List<Integer> r = new ArrayList<>();
        List<Integer> g = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        for (int elem : elementsList) {
            if (elem == 0) {
                r.add(0);
                g.add(0);
                b.add(0);
            } else {
                r.add(Color.red(elem));
                g.add(Color.green(elem));
                b.add(Color.blue(elem));
            }
        }


        return Color.rgb(mo.minElemOfList(r), mo.minElemOfList(g),mo.minElemOfList(b));
    }
    /**
     * wybiera medianowe wartosci
     * @param elementsList lista punktów maski
     * @return przetworzony punkt
     */
    public int medianPoint(List<Integer> elementsList) {
        List<Integer> r = new ArrayList<>();
        List<Integer> g = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        for (int elem : elementsList) {
            if (elem == 0) {
                r.add(0);
                g.add(0);
                b.add(0);
            } else {
                r.add(Color.red(elem));
                g.add(Color.green(elem));
                b.add(Color.blue(elem));
            }
        }
        //sort
       Collections.sort(r);
       Collections.sort(g);
       Collections.sort(b);
        return Color.rgb(r.get(4),g.get(4),b.get(4));
    }

    /**
     * przekształcenie z uśrednieniem ważonym
     * @param elementsList lista punktów potrzebnych do maski
     * @param mask maska
     * @return przetworzony punkt
     * @throws IllegalArgumentException  lista elementow ma różną wielkość od maski
     */
    public int pointAverage(List<Integer> elementsList,int mask[]) throws IllegalArgumentException
    {
        if(elementsList.size()!=mask.length)
        {
            throw new IllegalArgumentException();
        }
        int r=0;
        int g=0;
        int b=0;
        int numberOfFaktor=0;
        for(int i=0;i<mask.length;i++) {
            if(mask[i]<0){
                throw new IllegalArgumentException();
            }
            numberOfFaktor+=mask[i];
        }
        if(numberOfFaktor==0)
        {
            throw new IllegalArgumentException();
        }
        for (int elem : elementsList)
        {
            r+=Color.red(elem);
            g+=Color.green(elem);
            b+=Color.blue(elem);
        }
        r/=numberOfFaktor;
        g/=numberOfFaktor;
        b/=numberOfFaktor;
        return Color.rgb(r,g,b);
    }

    /**
     * przeksztalcenie z uzyciem dowlonej maski z normalizacja
     * @param elementsList lista punktów potrzebnych do maski
     * @param mask maska
     * @return przetworzony punkt
     * @throws IllegalArgumentException  lista elementow ma różną wielkość od maski
     */
    public int pointUseMask(List<Integer> elementsList,int mask[]) throws IllegalArgumentException
    {
        if(elementsList.size()!=mask.length)
        {
            throw new IllegalArgumentException();
        }
        int r=0;
        int g=0;
        int b=0;
        int index=0;
        for (int elem : elementsList) {
            r+=(Color.red(elem)*mask[index]);
            g+=(Color.green(elem)*mask[index]);
            b+=(Color.blue(elem)*mask[index]);
            index++;
        }
        r=rgbNormalization(r);
        g=rgbNormalization(g);
        b=rgbNormalization(b);

        return Color.rgb(r,g,b);
    }

    /**
     *
     * @param value wartosc pixela
     * @return znormalizowana wartość pixela
     */


    public int rgbNormalization(int value)
    {
        value=Math.abs(value);
    if(value>255)
    {
        value=255;
    }

        return value;
    }
}

