package com.project.contexttransform;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.FloatMath;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;



public class MainActivity extends ActionBarActivity {

    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    PointF startPoint = new PointF();
    PointF midPoint = new PointF();
    float oldDist = 1f;
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    private static String logTag="Program";
    private static final int OPEN_PHOTO = 1;
    private static final int TAKE_PHOTO =2;

    private static Bitmap bitmap;
    private static ImageView imageview;
    private Uri photoUri;
    private   Button takePhotoButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       takePhotoButton=(Button)findViewById(R.id.takePhoto);
        takePhotoButton.setOnClickListener(cameraListener);

        if(bitmap==null)
            bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.lena);
        imageview=(ImageView)findViewById(R.id.imageCamera);
        refreshBitmap();
        imageZoom();

    }

    private View.OnClickListener cameraListener = new View.OnClickListener() {
        public void onClick(View view) {

            captureCameraPhoto();
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id)
        {


            case R.id.action_default:
                bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.lena);
                refreshBitmap();
                return true;
            case R.id.action_openWithFile:
                openImageWithCardMemory();
                return true;
            case R.id.action_saveFile:
                openSaveImage();
                return true;
            case R.id.action_filters:
                openFiltersActivity();
                return true;
            case R.id.action_point:
                openPointActivity();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);
        switch(requestCode) {

            case TAKE_PHOTO:

                if (resultCode == Activity.RESULT_OK) {

                    //getContentResolver().notifyChange(photoUri, null);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri);
                        imageview.setImageBitmap(bitmap);
                        Toast.makeText(MainActivity.this, "" + bitmap.getWidth() + " x " + bitmap.getHeight(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Log.e(logTag, e.toString());
                    }
                }
                break;

            case OPEN_PHOTO:
                if(resultCode == RESULT_OK){
                    try {
                        Uri selectedImage = data.getData();
                        InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                        bitmap = BitmapFactory.decodeStream(imageStream);
                        Toast.makeText(MainActivity.this, "" + bitmap.getWidth() + " x " + bitmap.getHeight(), Toast.LENGTH_LONG).show();
                        refreshBitmap();
                    }catch(FileNotFoundException fnfe){
                        Toast.makeText(MainActivity.this, "Otwarcie nie udane", Toast.LENGTH_LONG).show();
                    }
                }
        }

    }
    public void openFiltersActivity()
    {
        Intent intent= new Intent("com.project.contexttransform.FiltersActivity");
        startActivity(intent);
    }
    public void openPointActivity()
    {
        Intent intent= new Intent("com.project.contexttransform.PointActivity");
        startActivity(intent);
    }
    public void openSaveImage()
    {
       Intent intent=new Intent("com.project.contexttransform.SaveImageActivity");
       startActivity(intent);

    }
    public void captureCameraPhoto()
    {

        //uzycie apki to zdjec
        Intent intent=new Intent("android.media.action.IMAGE_CAPTURE");
        //tymczasowe miejsce skladowania zdjecia
        File file =new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"temp.jpg");
        photoUri=Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
        //uruchomienie kamery
        startActivityForResult(intent, TAKE_PHOTO);

    }
    void openImageWithCardMemory()
    {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, OPEN_PHOTO);
    }

    public static void refreshBitmap()
    {
        imageview.setImageBitmap(bitmap);
    }
    public static void setBitmap(Bitmap map)
    {
        bitmap=map;
    }
    public static Bitmap getBitmap()
    {
        return bitmap;
    }


    void imageZoom()
    {
        imageview.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ImageView view = (ImageView) v;
                System.out.println("matrix=" + savedMatrix.toString());
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:

                        savedMatrix.set(matrix);
                        startPoint.set(event.getX(), event.getY());
                        mode = DRAG;
                        break;

                    case MotionEvent.ACTION_POINTER_DOWN:

                        oldDist = spacing(event);

                        if (oldDist > 10f) {
                            savedMatrix.set(matrix);
                            midPoint(midPoint, event);
                            mode = ZOOM;
                        }
                        break;

                    case MotionEvent.ACTION_UP:

                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;

                        break;

                    case MotionEvent.ACTION_MOVE:
                        if (mode == DRAG) {
                            matrix.set(savedMatrix);
                            matrix.postTranslate(event.getX() - startPoint.x,
                                    event.getY() - startPoint.y);
                        } else if (mode == ZOOM) {
                            float newDist = spacing(event);
                            if (newDist > 10f) {
                                matrix.set(savedMatrix);
                                float scale = newDist / oldDist;
                                matrix.postScale(scale, scale, midPoint.x, midPoint.y);
                            }
                        }
                        break;

                }
                view.setImageMatrix(matrix);

                return true;
            }

            @SuppressLint("FloatMath")
            private float spacing(MotionEvent event) {
                float x = event.getX(0) - event.getX(1);
                float y = event.getY(0) - event.getY(1);
                return FloatMath.sqrt(x * x + y * y);
            }

            private void midPoint(PointF point, MotionEvent event) {
                float x = event.getX(0) + event.getX(1);
                float y = event.getY(0) + event.getY(1);
                point.set(x / 2, y / 2);
            }
        });

    }

}