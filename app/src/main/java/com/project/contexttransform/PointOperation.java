package com.project.contexttransform;

import android.graphics.Color;


public class PointOperation {
    public PointOperation() {
    }

    /**
     * Tworzy negatyw obrazu
     * @param point wartosc koloru
     * @return nowa wartość koloru
     */
    public int pointNegative(int point)
    {
        int r,g,b;
        r= 255-Color.red(point);
        g=255-Color.green(point);
        b=255-Color.blue(point);
        point=Color.rgb(r,g,b);
        return point;
    }
    /**
     * Tworzy czarno biały obraz obrazu
     * @param point wartosc koloru
     * @return nowa wartość koloru
     */
    public int pointBlackAndWhite(int point)
    {

        int average;
        average=(Color.red(point)+Color.green(point)+Color.blue(point))/3;
        point=Color.rgb(average,average,average);
        return point;
    }
    /**
     * Tworzy sepie obrazu
     * @param point wartosc koloru
     * @param w współczynnik sepi
     * @return nowa wartość koloru
     */
    public int pointSepia(int point,int w)
    {
        int r,g,b;
        int average;
        average=(Color.red(point)+Color.green(point)+Color.blue(point))/3;
        r=average+2*w;
        if(r>255) r=255;
        g=average+w;
        b=average;
        if(g>255) g=255;
        point=Color.rgb(r,g,b);
        return point;
    }

    /**
     * Wygasza wybrane kolory
     * @param point wartosc koloru
     * @param rBool wygas czerwony
     * @param gBool wygaś zielony
     * @param bBool wygaś niebieski
     * @return nowa wartość koloru
     */
    public int pointFade(int point,boolean rBool,boolean gBool,boolean bBool)
    {
        int r,g,b;
        if(rBool) r=0;
        else r=Color.red(point);
        if(gBool)g=0;
        else g=Color.green(point);
        if(bBool) b=0;
        else b=Color.blue(point);


        point=Color.rgb(r,g,b);
        return point;
    }

}
