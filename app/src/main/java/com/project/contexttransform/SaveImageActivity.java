package com.project.contexttransform;

import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;


public class SaveImageActivity extends ActionBarActivity {

    private Button button, returnButton;
    private EditText name;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_image);
        bitmap = MainActivity.getBitmap();
        listeners();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_return) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void listeners() {
        button = (Button) findViewById(R.id.saveButton);
        name = (EditText) findViewById(R.id.namePlain);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName = "" + name.getText() + "";
                savePhoto(bitmap, fileName);
            }
        });
        returnButton = (Button) findViewById(R.id.returnButon);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void savePhoto(Bitmap bitmap, String nameFile) {
        OutputStream output;
        File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath() + "/ProjektSaveImage");
        dir.mkdirs();
        File file = new File(dir, nameFile + ".jpg");

        try {
            output = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
            Toast.makeText(SaveImageActivity.this, "Plik sie zapisuje: " + nameFile + ".jpg", Toast.LENGTH_LONG).show();
            output.flush();
            output.close();

        } catch (Exception e) {
            Toast.makeText(SaveImageActivity.this, "Kompresja nie udana", Toast.LENGTH_SHORT).show();
        }
    }
}