package com.project.contexttransform;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


public class PointActivity extends ActionBarActivity {

    private final int idNegative=1001;
    private final int idBlackAndWhite=1002;

    private Button bNegative,bBlackAndWhite,bSepia,bFading;
    private ProgressBar progress;


    private Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point);
        bitmap=MainActivity.getBitmap();
        listen();
    }

    private void listen()
    {


        progress=(ProgressBar)findViewById(R.id.progressBar1);
        bNegative=(Button)findViewById(R.id.buttonNegative);
        bNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoPointTransform trans=new DoPointTransform(idNegative);
                trans.execute();
                //finish();
            }
        });

        bBlackAndWhite=(Button)findViewById(R.id.buttonBAW);
        bBlackAndWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoPointTransform trans=new DoPointTransform(idBlackAndWhite);
                trans.execute();
            }
        });
        bSepia=(Button)findViewById(R.id.buttonSepia);
        bSepia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent("com.project.contexttransform.SepiaActivity");
                startActivity(intent);
                finish();
            }
        });
        bFading=(Button)findViewById(R.id.buttonFade);
        bFading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent("com.project.contexttransform.FadeActivity");
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_point, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_return) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class DoPointTransform extends AsyncTask<Void, Void, Void>
    {
        int id_Operation;
        int pointNumber;
        int pointCounter;
        PointOperation po;
        Bitmap resultBitmap;
        DoPointTransform(int id_Operation) {
            this.id_Operation = id_Operation;
            pointNumber=bitmap.getWidth()*bitmap.getHeight();
            pointCounter=0;
            po=new PointOperation();
            resultBitmap=bitmap.copy(Bitmap.Config.ARGB_8888, true);
        }

        @Override
        protected Void doInBackground(Void... arg)
        {

            for(int i=0;i<bitmap.getWidth();i++) {
                for (int j = 0; j < bitmap.getHeight(); j++) {
                    switch (id_Operation)
                    {
                        case idNegative:
                            resultBitmap.setPixel(i,j,po.pointNegative(resultBitmap.getPixel(i,j)));
                            break;
                        case idBlackAndWhite:
                            resultBitmap.setPixel(i,j,po.pointBlackAndWhite(resultBitmap.getPixel(i,j)));
                        default:
                            break;
                    }


                }

            }
            return null;
        }
        @Override
        protected void onPreExecute()
        {
            bNegative.setEnabled(false);
            bBlackAndWhite.setEnabled(false);
            bSepia.setEnabled(false);
            bFading.setEnabled(false);
            progress.setVisibility(ProgressBar.VISIBLE);

            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(Void result) {
            MainActivity.setBitmap(resultBitmap);
            MainActivity.refreshBitmap();

            finish();
            super.onPostExecute(result);
        }
        @Override
        protected void onProgressUpdate(Void... values)
        {

            super.onProgressUpdate(values);
        }

    }

}
