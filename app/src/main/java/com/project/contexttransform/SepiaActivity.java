package com.project.contexttransform;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ProgressBar;


public class SepiaActivity extends ActionBarActivity {
    Bitmap bitmap;
    private ProgressBar stan;
    private Button akcept;
    private NumberPicker pick;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Choose Factor");
        setContentView(R.layout.activity_sepia);

        declaration();
        bitmap=MainActivity.getBitmap();
    }

    public void declaration() {
        pick = (NumberPicker) findViewById(R.id.numberPicker);
        akcept = (Button) findViewById(R.id.akcept);
        stan = (ProgressBar) findViewById(R.id.progressBar);
        pick.setMaxValue(40);
        pick.setMinValue(20);
        stan.setVisibility( ProgressBar.INVISIBLE);
        akcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DoSepia ob=new DoSepia(pick.getValue());
                ob.execute();


            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sepia, menu);
        return true;
    }







    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_return) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class DoSepia extends AsyncTask<Void, Void, Void>
    {
        private int factor;
        PointOperation po;
        Bitmap resultBitmap;

        DoSepia(int factor) {
            this.factor = factor;
            po=new PointOperation();
            resultBitmap=bitmap.copy(Bitmap.Config.ARGB_8888, true);
        }

        @Override
        protected Void doInBackground(Void... arg)
        {
            for(int i=0;i<bitmap.getWidth();i++) {
                for (int j = 0; j < bitmap.getHeight(); j++) {

                            resultBitmap.setPixel(i,j,po.pointSepia(resultBitmap.getPixel(i,j),factor));
                    }


                }


            return null;
        }
        @Override
        protected void onPreExecute()
        {
            stan.setVisibility( ProgressBar.VISIBLE);
            akcept.setEnabled(false);
            pick.setEnabled(false);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(Void result) {
            MainActivity.setBitmap(resultBitmap);
            MainActivity.refreshBitmap();
            finish();
            super.onPostExecute(result);
        }
        @Override
        protected void onProgressUpdate(Void... values)
        {

            super.onProgressUpdate(values);
        }

    }
}