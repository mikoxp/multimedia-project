package com.project.contexttransform;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;


public class FadeActivity extends ActionBarActivity {

    private CheckBox red,green,blue;
    private Button button;
    private ProgressBar progress;
    private Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fade);
        bitmap=MainActivity.getBitmap();
        lisinners();

    }
    private void lisinners()
    {
        red=(CheckBox)findViewById(R.id.checkRed);
        blue=(CheckBox)findViewById(R.id.checkBlue);
        green=(CheckBox)findViewById(R.id.checkGreen);
        progress=(ProgressBar)findViewById(R.id.progressBar);
        button=(Button)findViewById(R.id.fadeButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFading doing=new DoFading(red.isChecked(),green.isChecked(),blue.isChecked());
                doing.execute();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fade, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_return) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class DoFading extends AsyncTask<Void, Void, Void>
    {
        boolean redBool;
        boolean greenBool;
        boolean blueBool;
        PointOperation po;
        Bitmap resultBitmap;

        DoFading(boolean redBool, boolean greenBool, boolean blueBool) {
            this.redBool = redBool;
            this.greenBool = greenBool;
            this.blueBool = blueBool;
            po=new PointOperation();
            resultBitmap=bitmap.copy(Bitmap.Config.ARGB_8888, true);
        }

        @Override
        protected Void doInBackground(Void... arg)
        {

            for(int i=0;i<bitmap.getWidth();i++) {
                for (int j = 0; j < bitmap.getHeight(); j++) {

                    resultBitmap.setPixel(i,j,po.pointFade(resultBitmap.getPixel(i,j),redBool,greenBool,blueBool));
                }


            }
            return null;
        }
        @Override
        protected void onPreExecute()
        {
            progress.setVisibility( ProgressBar.VISIBLE);
            button.setEnabled(false);
            red.setEnabled(false);
            blue.setEnabled(false);
            green.setEnabled(false);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(Void result) {
            MainActivity.setBitmap(resultBitmap);
            MainActivity.refreshBitmap();
            finish();
            super.onPostExecute(result);
        }
        @Override
        protected void onProgressUpdate(Void... values)
        {

            super.onProgressUpdate(values);
        }

    }
}
