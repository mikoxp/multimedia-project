package com.project.contexttransform;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by miko on 2015-05-17.
 */
public class MathsOperation {
    public MathsOperation() {
    }

    /**
     *
     * @param listOfElements lista elementów całkowitych
     * @return najwiekszy element z listy
     */
    public int maxElemOfList(List<Integer> listOfElements)
    {
        int max=listOfElements.get(0);
        for(int elem:listOfElements)
        {
            if(max<elem)
                max=elem;
        }
        return max;
    }
    /**
     *
     * @param listOfElements lista elementów całkowitych
     * @return najmniejszy element z listy
     */
    public int minElemOfList( List<Integer> listOfElements)
    {
        int min=listOfElements.get(0);
        for(int elem:listOfElements)
        {
            if(min>elem)
                min=elem;
        }
        return min;
    }

    /**
     *
     * @param tab tablica wartosci całkowitych
     * @return najwieksza wartosc
     */
    public int maxElemOfArray(int tab[])
    {
        int elem=tab[0];
        for(int i=1;i<tab.length;i++){
            if(elem<tab[i])
                elem=tab[i];
        }
        return elem;

    }
    /**
     *
     * @param tab tablica wartosci całkowitych
     * @return najwieksza wartosc
     */
    public int minElemOfArray(int tab[])
    {
        int elem=tab[0];
        for(int i=1;i<tab.length;i++){
            if(elem>tab[i])
                elem=tab[i];
        }
        return elem;

    }

}
