package com.project.contexttransform;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class FiltersActivity extends ActionBarActivity {



    Bitmap bitmap;
    Button bMax;
    Button bMin;
    Button bMedian;
    Button bAverage;
    Button bLaplas;
    Button bRoberts;
    Button bPrewitt;
    ProgressBar stan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        bitmap=MainActivity.getBitmap();
       lis();
    }

    public void lis()
    {
        bMax =(Button)findViewById(R.id.bMax);
        bMax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFilter df = new DoFilter(DoFilter.id_max);
                df.execute();
            }
        });
        bMin=(Button)findViewById(R.id.bMin);
        bMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFilter df = new DoFilter(DoFilter.id_min);
                df.execute();
            }
        });
        bMedian=(Button)findViewById(R.id.bMedian);
        bMedian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFilter df = new DoFilter(DoFilter.id_median);
                df.execute();
            }
        });
        bAverage=(Button)findViewById(R.id.bAverage);
        bAverage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFilter df = new DoFilter(DoFilter.id_average);
                df.execute();
            }
        });
        bLaplas=(Button)findViewById(R.id.bLaplas);
        bLaplas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFilter df = new DoFilter(DoFilter.id_laplas);
                df.execute();
            }
        });
        bRoberts=(Button)findViewById(R.id.bRoberts);
        bRoberts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFilter df = new DoFilter(DoFilter.id_Roberts);
                df.execute();
            }
        });
        bPrewitt=(Button)findViewById(R.id.bPrewitt);
        bPrewitt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoFilter df = new DoFilter(DoFilter.id_Prewitt);
                df.execute();
            }
        });
        stan=(ProgressBar)findViewById(R.id.progressBar1);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filters, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_return) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class DoFilter extends AsyncTask<Void, Void, Void> {

        public static final int id_max=1;
        public static final int id_min=2;
        public static final int id_median=3;
        public static final int id_average=4;
        public  static final int id_laplas=5;
        public static final int id_Roberts=6;
        public static final int id_Prewitt=7;

        private int maskAverage[]={1,1,1,1,1,1,1,1,1};
        private int maskLaplas[]={-1,-1,-1,-1,8,-1,-1,-1,-1};
        private int maskRoberts[]={0,-1,0,0,0,1,0,0,0};
        private int maskPrewitt[]={-1,-1,-1,0,0,0,1,1,1};

        int id_Operation;
        Bitmap map;
        FilterOperation fo;
        MathsOperation mo;

        DoFilter(int id_Operation) {
            fo = new FilterOperation();
            mo = new MathsOperation();
            this.id_Operation=id_Operation;
        }

        @Override
        protected Void doInBackground(Void... arg) {
            int elem = Color.BLACK;
            map = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.RGB_565);
            List<Integer> elementsList = new ArrayList<>();
            try {
                for (int i = 0; i < bitmap.getWidth(); i++) {
                    for (int j = 0; j < bitmap.getHeight(); j++) {
                        elementsList = fo.choose9ElementsMask(bitmap, i, j, elementsList);
                        switch (id_Operation) {
                            case id_max:
                                elem = fo.maxPoint(elementsList);
                                break;
                            case id_min:
                                elem = fo.minPoint(elementsList);
                                break;
                            case id_median:
                                elem = fo.medianPoint(elementsList);
                                break;
                            case id_average:
                                elem=fo.pointAverage(elementsList,maskAverage);
                                break;
                            case id_laplas:
                                elem=fo.pointUseMask(elementsList,maskLaplas);
                                break;
                            case id_Roberts:
                                elem=fo.pointUseMask(elementsList,maskRoberts);
                                break;
                            case id_Prewitt:
                                elem=fo.pointUseMask(elementsList,maskPrewitt);
                            default:

                                break;
                        }

                        map.setPixel(i, j, elem);
                    }
                }
            }catch(Exception e){
                Log.v("Filters",e.toString());
                Toast.makeText(FiltersActivity.this,"exception",Toast.LENGTH_LONG).show();

            }
                return null;
        }

        @Override
        protected void onPreExecute() {
            stan.setVisibility(ProgressBar.VISIBLE);
            bMax.setEnabled(false);
            bMin.setEnabled(false);
            bMedian.setEnabled(false);
            bAverage.setEnabled(false);
            bRoberts.setEnabled(false);
            bLaplas.setEnabled(false);
            bPrewitt.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            MainActivity.setBitmap(map);
            MainActivity.refreshBitmap();
            finish();
            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(Void... values) {

            super.onProgressUpdate(values);
        }



    }
}
